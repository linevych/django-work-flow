import sys, os
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'deploy'))
from fabfile import *

def my_deploy():
    env.domain = '{project}'
    env.repository = 'https://{bit_user}@bitbucket.org/{bit_user}/{project}.git'

    deploy()

if __name__ == '__main__':
    # hack for pycharm run configuration.
    import subprocess, sys
    subprocess.call(['fab', '-f', __file__] + sys.argv[1:])